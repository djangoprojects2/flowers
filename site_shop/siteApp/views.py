import json
from collections import Counter
from datetime import timedelta
from itertools import combinations

from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import View
from django.views.generic import TemplateView, CreateView, UpdateView

from .forms import RegisterUserForm, LoginUserForm
from .models import Product, Order, Person, Feedback, Category, Cart, CartItem, Inventory, OrderItem
from django.db.models import Count


class IndexView(TemplateView):
    template_name = 'siteApp/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Проверка и обновление статусов заказов
        current_time = timezone.now()
        orders_to_update = Order.objects.filter(order_date__lt=current_time, status=False)
        for order in orders_to_update:
            order.status = True  # Устанавливаем статус "Доставлено"
            order.save()

        # Обновление статуса наличия товаров
        Product.objects.filter(inventory__quantity=0).update(is_active=False)
        Product.objects.filter(inventory__quantity__gt=0, is_active=False).update(is_active=True)

        # Получаем продукты с их количеством заказов
        products_ordered_count = Product.objects.annotate(
            orders_count=Count('order')
        ).order_by('-orders_count')

        if self.request.user.is_authenticated:
            cart = Cart.objects.filter(person=self.request.user).first()
            if cart:
                context['cart_items'] = [item.product.id for item in cart.items.all()]
            else:
                context['cart_items'] = []
        else:
            context['cart_items'] = []

        context['popular_products'] = products_ordered_count[:8]

        latest_feedbacks = Feedback.objects.select_related('product', 'person').order_by('-id')[:3]
        context['latest_feedbacks'] = latest_feedbacks
        return context


class RegisterUser(CreateView):
    form_class = RegisterUserForm
    template_name = 'siteApp/register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Регистрация"
        return context

    def form_valid(self, form):
        user = form.save(commit=False)  # сохраняем пользователя, но пока без коммита в БД
        user.set_password(form.cleaned_data['password1'])  # устанавливаем и хешируем пароль
        user.save()  # сохраняем пользователя в БД
        login(self.request, user)  # авторизуем пользователя
        print("Регистрация прошла успешно. Добро пожаловать!")
        return redirect('home')


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = 'siteApp/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Авторизация"
        return context

    def get_success_url(self):
        return reverse_lazy('home')


class ProductListView(TemplateView):
    template_name = 'siteApp/product_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Получаем список всех категорий
        categories = Category.objects.all()
        context['categories'] = categories

        category_id = self.request.GET.get('category')
        search_query = self.request.GET.get('search')
        availability = self.request.GET.get('availability')
        min_price = self.request.GET.get('min_price')
        max_price = self.request.GET.get('max_price')
        reset_filters = self.request.GET.get('reset_filters')

        apply_filters = self.request.GET.get('apply_filters')
        if apply_filters:
            # Сбрасываем значения min_price и max_price
            context['min_price'] = None
            context['max_price'] = None
        else:
            # Сохраняем текущие значения для отображения в форме
            context['min_price'] = self.request.GET.get('min_price')
            context['max_price'] = self.request.GET.get('max_price')

        products = Product.objects.all()

        if reset_filters:
            # Сброс всех фильтров, кроме фильтра категории
            products = Product.objects.filter(category_id=category_id)
        else:
            # Приоритет у поискового запроса
            if search_query:
                products = products.filter(title__icontains=search_query)
            elif category_id:
                products = products.filter(category_id=category_id)
            elif not category_id and categories.exists():
                # Если категория не выбрана, используем первую доступную
                products = products.filter(category_id=categories.first().id)

            # Применение остальных фильтров
            if availability != "" and availability is not None:
                products = products.filter(is_active=availability == "1")
            if min_price:
                products = products.filter(price__gte=min_price)
            if max_price:
                products = products.filter(price__lte=max_price)

        if self.request.user.is_authenticated:
            cart = Cart.objects.filter(person=self.request.user).first()
            if cart:
                context['cart_items'] = [item.product.id for item in cart.items.all()]
            else:
                context['cart_items'] = []
        else:
            context['cart_items'] = []

        context['products'] = products
        return context


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'siteApp/profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Получаем информацию о текущем пользователе
        user = get_object_or_404(Person, username=self.request.user.username)
        context['user'] = user

        # Получаем заказы пользователя
        orders = Order.objects.filter(person=user).order_by('-order_date')
        for order in orders:
            # Расчет времени до доставки
            time_left = order.order_date - timezone.now()
            # Проверка, остался ли меньше часа до доставки
            order.can_modify = time_left > timedelta(hours=1)

            product_reviews = {}
            for product in order.products.all():
                has_review = Feedback.objects.filter(person=user, product=product).exists()
                review_rating = Feedback.objects.filter(person=user,
                                                        product=product).first().rating if has_review else None
                product_reviews[product.id] = {'has_review': has_review, 'review_rating': review_rating}

            # Добавляем словарь к объекту заказа
            order.product_reviews = product_reviews

        context['orders'] = orders
        return context


class OrderEditView(View):
    def post(self, request, order_id):
        order = get_object_or_404(Order, id=order_id, person=request.user, status=False)
        order.delivery_address = request.POST.get('delivery_address')
        order.order_date = request.POST.get('order_date')
        order.save()
        return redirect('profile')


class CancelOrderView(LoginRequiredMixin, View):
    def post(self, request, order_id):
        order = get_object_or_404(Order, id=order_id, person=request.user)

        if not order.status:
            # Перебор всех элементов заказа и возврат товаров на склад
            for item in order.order_items.all():
                inventory_item = Inventory.objects.get(product=item.product)
                inventory_item.quantity += item.quantity
                inventory_item.save()

            # Удаление заказа
            order.delete()

        return redirect('profile')


class ReviewCreateView(LoginRequiredMixin, CreateView):
    model = Feedback
    fields = ['rating', 'comment']
    template_name = 'siteApp/review.html'

    def form_valid(self, form):
        form.instance.person = self.request.user
        form.instance.product = get_object_or_404(Product, pk=self.kwargs['product_id'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('profile')


class AddToCartView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        product_id = data.get('product_id')
        product = get_object_or_404(Product, id=product_id)
        cart, _ = Cart.objects.get_or_create(person=request.user)
        cart_item, created = CartItem.objects.get_or_create(
            person=request.user,
            product=product
        )
        if created:
            cart.items.add(cart_item)
        cart.update_total_price()

        return JsonResponse({'status': 'success'})


class CartView(LoginRequiredMixin, TemplateView):
    template_name = 'siteApp/cart.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart, _ = Cart.objects.get_or_create(person=self.request.user)
        context['cart'] = cart
        return context


class RemoveFromCartView(View):
    def post(self, request, item_id):
        cart_item = get_object_or_404(CartItem, id=item_id, person=request.user)
        cart = Cart.objects.get(person=request.user)
        cart.items.remove(cart_item)
        cart_item.delete()
        # Пересчитать общую стоимость корзины
        cart.total_price = sum(item.product.price * item.quantity for item in cart.items.all())
        cart.save()
        return redirect('cart')


class ChangeCartItemQuantityView(View):
    def post(self, request, item_id):
        quantity = request.POST.get('quantity', 1)
        cart_item = get_object_or_404(CartItem, id=item_id, person=request.user)
        quantity = int(quantity)

        inventory_item = Inventory.objects.get(product=cart_item.product)

        if quantity <= 0:
            cart_item.delete()
            # Если товар удален, нужно пересчитать общую стоимость корзины
            cart = Cart.objects.get(person=request.user)
            cart.total_price = sum(item.product.price * item.quantity for item in cart.items.all())
            cart.save()
            return JsonResponse({
                'status': 'item_deleted',
                'cart_total': cart.total_price,
                'cart_items_count': cart.items.count()
            })
        elif quantity > inventory_item.quantity:
            return JsonResponse({
                'status': 'error',
                'message': 'На складе недостаточно товара.',
                'current_quantity': cart_item.quantity
            })
        else:
            cart_item.quantity = quantity
            cart_item.save()
            cart = Cart.objects.get(person=request.user)
            cart.total_price = sum(item.product.price * item.quantity for item in cart.items.all())
            cart.save()
            return JsonResponse({
                'status': 'success',
                'item_id': item_id,
                'new_total': cart_item.product.price * cart_item.quantity,
                'cart_total': cart.total_price,
                'cart_items_count': cart.items.count()
            })


class ClearCartView(View):
    def post(self, request):
        cart = Cart.objects.get(person=request.user)
        # Удаляем сами объекты CartItem
        for item in cart.items.all():
            item.delete()
        cart.items.clear()  # Это уже не обязательно, но можно оставить для ясности
        cart.total_price = 0
        cart.save()
        return redirect('cart')


class CreateOrderView(LoginRequiredMixin, View):
    def get(self, request):
        cart = Cart.objects.get(person=request.user)
        return render(request, 'siteApp/create_order.html', {'cart': cart})

    def post(self, request):
        cart = Cart.objects.get(person=request.user)
        address = request.POST.get('address')
        delivery_time = request.POST.get('delivery_time')

        # Создание заказа
        order = Order.objects.create(
            person=request.user,
            delivery_address=address,
            order_date=delivery_time,
            total_price=cart.total_price
        )

        # Добавление товаров в заказ и обновление инвентаря
        for item in cart.items.all():
            order.products.add(item.product)
            OrderItem.objects.create(
                order=order,
                product=item.product,
                quantity=item.quantity
            )

            inventory_item = Inventory.objects.get(product=item.product)
            inventory_item.quantity -= item.quantity
            inventory_item.save()

            # Удаление элемента корзины
            item.delete()

        cart.update_total_price()

        return redirect('home')


# функции анализа

#Определение товаров, которые часто покупаются вместе
def find_frequently_bought_together():
    pairs_counter = Counter()

    for order in Order.objects.all():
        products_in_order = list(order.products.all())
        for pair in combinations(products_in_order, 2):
            pairs_counter[pair] += 1

    most_common_pairs = pairs_counter.most_common(5)
    print("Товары, часто покупаемые вместе:")
    for pair, count in most_common_pairs:
        print(f"{pair[0].title} и {pair[1].title}, Количество: {count}")


#find_frequently_bought_together()


#Оценка запасов и спроса
def assess_stock_and_demand():
    for product in Product.objects.all():
        inventory = Inventory.objects.get(product=product)
        orders_count = product.order_set.count()
        print(f"Товар: {product.title}, В наличии: {inventory.quantity}, Количество заказов: {orders_count}")


#assess_stock_and_demand()


#Анализ отзывов

def analyze_feedbacks():
    feedbacks = Feedback.objects.all()
    if feedbacks.exists():
        total_rating = sum(feedback.rating for feedback in feedbacks)
        average_rating = total_rating / feedbacks.count()

        print(f"Средняя оценка отзывов: {average_rating:.2f}")

        if average_rating >= 4:
            print("Клиенты в основном довольны работой.")
        else:
            print("Клиенты в основном не довольны работой.")
    else:
        print("Отзывы отсутствуют.")


#analyze_feedbacks()