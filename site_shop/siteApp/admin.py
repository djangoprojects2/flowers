from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['name']


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    search_fields = ['title']


admin.site.register(Product, ProductAdmin)
admin.site.register(Feedback)
admin.site.register(Discounts)


class OrderAdmin(admin.ModelAdmin):
    search_fields = ['products__title']


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem)
admin.site.register(Inventory)

admin.site.site_title = 'Админ-панель сайта'
admin.site.site_header = 'Админ-панель сайта'


class CustomUserAdmin(UserAdmin):
    model = Person
    fieldsets = UserAdmin.fieldsets + (
            (None, {'fields': ('phone', 'role')}),
        )


admin.site.register(Person, CustomUserAdmin)
admin.site.register(Cart)
admin.site.register(CartItem)
