document.addEventListener('DOMContentLoaded', function() {
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    document.querySelectorAll('.cart-item-quantity').forEach(function(inputField) {
        inputField.addEventListener('change', function() {
            var itemId = this.dataset.itemId;
            var quantity = this.value;
            var removeUrl = '/cart/remove/' + itemId + '/';
            var changeUrl = '/cart/change-quantity/' + itemId + '/';
            // Проверка, является ли новое количество нулем
            if (quantity <= 0) {
                // Отображение диалога подтверждения
                if (confirm('Вы хотите удалить товар из корзины?')) {
                    // AJAX-запрос на удаление
                    fetch(removeUrl, {
                        method: 'POST',
                        headers: {
                            'X-CSRFToken': getCookie('csrftoken')
                        }
                    })
                    .then(response => {
                        if (response.ok) {
                            location.reload(); // Перезагрузка страницы после удаления товара
                        }
                    });
                } else {
                    // Возвращение предыдущего значения, если пользователь отменяет удаление
                    this.value = this.defaultValue;
                }
            } else {
                fetch(changeUrl, {
                    method: 'POST',
                    headers: {
                        'X-CSRFToken': getCookie('csrftoken'),
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    body: 'quantity=' + quantity
                })
                .then(response => response.json())
                .then(data => {
                    if (data.status === 'success') {
                        location.reload(); // Перезагрузка страницы после успешного изменения
                    } else if (data.status === 'error') {
                        alert(data.message); // Показать сообщение об ошибке
                        document.querySelector(`[data-item-id="${itemId}"]`).value = data.current_quantity; // Вернуть текущее количество
                    }
                });
            }
        });
    });
});