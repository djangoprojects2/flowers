function addToCart(buttonElement, productId) {
    var addUrl = '/add-to-cart/' + productId + '/';
    var csrftoken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch(addUrl, {
        method: 'POST',
        headers: {
            'X-CSRFToken': csrftoken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ product_id: productId })
    })
    .then(response => response.json())
    .then(data => {
        if (data.status === 'success') {
            buttonElement.textContent = 'Уже в корзине';
            buttonElement.classList.remove('btn-primary');
            buttonElement.classList.add('btn-secondary');
            buttonElement.disabled = true;
        }
    });
}