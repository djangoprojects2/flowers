from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.core.validators import MinValueValidator, MaxValueValidator


class Person(AbstractUser):
    first_name = models.CharField(max_length=50, verbose_name="Имя")
    last_name = models.CharField(max_length=50, verbose_name="Фамилия")
    phone = models.CharField(max_length=255, blank=True, null=True, unique=True, verbose_name="Номер телефона")
    email = models.EmailField(verbose_name="Почта")
    role = models.CharField(max_length=1, choices=[('C', 'Client'), ('S', 'Staff')], verbose_name="Роль")

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def has_reviewed(self, product):
        return Feedback.objects.filter(person=self, product=product).exists()

    def get_review_rating(self, product):
        review = Feedback.objects.filter(person=self, product=product).first()
        return review.rating if review else None


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Категория')
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Product(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, validators=[MinValueValidator(1)], verbose_name="Цена")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория')
    description = models.TextField(blank=True, verbose_name='Информация о услуге')
    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name='Фото')
    is_active = models.BooleanField(default=True, verbose_name='Наличие')

    def __str__(self):
        return f"{self.title}"

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'


class Feedback(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Пользователь')
    comment = models.TextField(verbose_name='Комментарий')
    rating = models.IntegerField(validators=[MaxValueValidator(5)], verbose_name='Оценка')

    def __str__(self):
        return f"Отзыв с рейтингом {self.rating} на товар '{self.product}'"

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


class Discounts(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')
    discount_rate = models.FloatField(verbose_name='Размер скидки')
    start_date = models.DateTimeField(verbose_name='Начало скидки')
    end_date = models.DateTimeField(verbose_name='Конец скидки')

    def __str__(self):
        return f"Скидка на {self.product}"

    class Meta:
        verbose_name = 'Скидка'
        verbose_name_plural = 'Скидки'


class Order(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, verbose_name='Пользователь')
    products = models.ManyToManyField(Product, verbose_name='Товары')
    close_date = models.DateTimeField(verbose_name='Дата заказа', auto_now_add=True)
    order_date = models.DateTimeField(verbose_name='Дата доставки')
    total_price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Стоимость')
    delivery_address = models.TextField(verbose_name='Адрес Доставки')
    status = models.BooleanField(default=False, verbose_name='Статус Заказа')

    def __str__(self):
        # Получаем названия первых нескольких товаров в заказе
        product_names = [product.title for product in self.products.all()[:3]]
        product_str = ', '.join(product_names)

        # Если товаров больше, чем отображаем, добавляем многоточие
        if self.products.count() > 3:
            product_str += '...'

        return f"Заказ ({product_str})"

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items', verbose_name='Заказ')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')
    quantity = models.IntegerField(default=1, verbose_name='Количество')

    def __str__(self):
        return f"{self.product.title} ({self.quantity}) в заказе {self.order.id}"

    class Meta:
        verbose_name = 'Элемент заказа'
        verbose_name_plural = 'Элементы заказов'


class Inventory(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Товар')
    quantity = models.IntegerField(verbose_name='Количество')

    def __str__(self):
        return f"Запасы {self.product}"

    class Meta:
        verbose_name = 'Инвентарь'
        verbose_name_plural = 'Инвентарь'


class CartItem(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    def __str__(self):
        return f"{self.product.title} ({self.quantity})"

    class Meta:
        verbose_name = 'Товар корзины'
        verbose_name_plural = 'Товары корзины'

    def total_price(self):
        return self.quantity * self.product.price


class Cart(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    items = models.ManyToManyField(CartItem)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    def __str__(self):
        return f"Корзина пользователя {self.person.username}"

    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзина'

    def update_total_price(self):
        total = 0
        for item in self.items.all():
            total += item.product.price * item.quantity
        self.total_price = total
        self.save()
