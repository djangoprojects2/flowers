from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib.auth.views import LogoutView

from .views import *

urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('products/', ProductListView.as_view(), name='product_list'),
    path('profile/', UserProfileView.as_view(), name='profile'),
    path('edit_order/<int:order_id>/', OrderEditView.as_view(), name='edit_order'),
    path('cancel-order/<int:order_id>/', CancelOrderView.as_view(), name='cancel_order'),
    path('product/<int:product_id>/review/', ReviewCreateView.as_view(), name='create_review'),
    path('add-to-cart/<int:product_id>/', AddToCartView.as_view(), name='add_to_cart'),
    path('cart/remove/<int:item_id>/', RemoveFromCartView.as_view(), name='remove_from_cart'),
    path('cart/change-quantity/<int:item_id>/', ChangeCartItemQuantityView.as_view(), name='change_quantity'),
    path('cart/clear/', ClearCartView.as_view(), name='clear_cart'),
    path('cart/', CartView.as_view(), name='cart'),
    path('create_order/', CreateOrderView.as_view(), name='create_order'),
]
